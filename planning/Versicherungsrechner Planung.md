# Versicherungsrechner Planung

Ich habe mich dafür entschieden einen Versicherungsrechner zu entwickeln, \
welcher berechnet ob es sich lohnt einen Schaden am Auto der Versicherung zu melden, oder ihn selbst zu bezahlen.

Beim planen des UIs bin ich darauf gekommen, dass es sich nicht lohnt Prämien für Hausratsversicherungen zu berechnen, \
da [comparis](https://comparis.ch) dies schon sehr gut tut. Ausserdem habe ich die Preise der Versicherungen nicht.

## Design

Im [Miro Board](https://miro.com/app/board/uXjVMcELHJI=/) habe ich folgendes getan:
- [Problem Statement](./screenshots/ProblemStatement.png)
- [Proto-Persona](./screenshots/ProtoPersonas.png)
- [User Story](./screenshots/UserStory.png)
- Key Screen Sketches ([Startseite](./Versicherungsrechner%20Sketch%20Startseite.jpg), [Rechner](Versicherungsrechner%20Sketch%20Rechner.jpg))

## System

![System](System.png)

### Frontend

Das Frontend möchte ich gerne mit [React](https://react.dev/) für den Browser und [Flutter](https://flutter.dev/) für Mobile Applikationen entwickeln. \
Ich werde das Frontend _Mobile First_ entwickeln, darum sind die Sketches auch Im Smartphone Format gezeichnet.

### Backend

Beim Backend bin ich mir noch unschlüssig. Vermutlich werde ich die BaaS Option wählen, weil ich mich für einmal mehr auf das Frontend konzentrieren möchte. \
Zudem bringen [Firebase](https://firebase.google.com/) und [PocketBase](https://pocketbase.io/) Authentifizierung von sich aus mit.

## Rechner

### Kontext

Fritz Meier hat eine Motorfahrzeug-Haftpflichtversicherung und ist auf dem Bonus-Malus-System auf **Prämienstufe 6**.

Zurzeit bezahlt er **3000 CHF pro Jahr**.

### Geschehen

Er hat einen selbstverbschuldeten Unfall und sein Auto erleidet **einen Schaden in der Höhe von 3000 CHF**.

Er fragt sich nun ob er:

- die **Versicherung den Schaden zahlen** lassen soll und somit auf dem Bonus-Malus-System **4 Stufen zurück** fällt.
- den **Schaden selbst bezahlt** und somit **auf der selben Prämienstufe bleibt**.

Genau diese Frage soll mein Versicherungsrechner beantworten.
