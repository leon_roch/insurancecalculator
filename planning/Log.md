# Versicherungsrechner Planungsübersicht

| Bezeichnung  |                         Angaben                          |
| ------------ | :------------------------------------------------------: |
| Autoren:     |                       Léonard Roch                       |
| Erstellt am: |                        24.3.2023                         |
| Git-Repo-URL | [Repo](https://gitlab.com/leon_roch/insurancecalculator) |

---

In diesem Dokument mache ich alle meine Tätigkeiten zum Projekt transparent.

- Liste mit allen Ideen, die in meinem Projekt noch anstehen
- Liste mit allen Ideen, die ich in einem deklarierten Zeitraum erledigen werde.
  - alles was geplant ist in diesem Zeitraum
  - alles was ich gerade mache
  - alles was bereits erledigt ist
- Liste aller zuvor abgeschlossener Arbeiten

Ich informiere regelmässig über meine Tätigkeiten.

---

## Liste der noch anstehenden Ideen

- [ ] Firebase App Check integrieren
- [ ] Flutter: Mobile Version entwickeln
- [ ] Mehrsprachigkeit hinzufügen

---

## Liste der Arbeiten, die ich am erledigen bin

- Zeitraum: 9.5.2023 - 12.5.2023

#### offen

#### in Arbeit

- [ ] Versicherungsauswahl der Nutzer auf FireStore speichern

#### erledigt

- [ ] Dark Mode hinzufügen

---

## Liste der zuvor abgeschlossenen Arbeiten

- [X] Dockerfile erstellen
- [x] Admin Panel entwickeln
- [x] Firebase Auth integrieren
- [x] Kernfunktion: Rechner programmieren
- [x] Versicherungsauswahl entwickeln
- [x] Routing aufsetzen
- [x] Firebase App integrieren
- [x] Firebase Backend aufsetzen
- [x] System Ideen visualisiert + Kernfunktionen des Rechners überlegt ([Ressourcen](./Versicherungsrechner%20Planung.md))
- [x] React Projekt aufsetzten (File Struktur einrichten)
- [x] Firebase Produkte gelernt
- [x] Repository fertig aufsetzten (readMe.md files anlegen)

_Template provided by: [Peter Rutschmann](mailto:peter.rutschmann@bbw.ch)_
