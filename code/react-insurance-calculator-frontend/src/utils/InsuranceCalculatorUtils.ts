import {
  AdditionReason,
  Calculation,
  ICalculation,
} from "../types/Calculation";
import { DamagePayedBy } from "../types/DamagePayedBy";
import { IInsurance, Insurance } from "../types/Insurance";

export function damagePaymentByInsurance(
  insurance: IInsurance,
  premium: number,
  currentLevel: number
): Calculation {
  const insuranceObject = new Insurance(insurance);

  const damagePremium = insuranceObject.damagePremium(premium, currentLevel);

  const calculation = new Calculation(DamagePayedBy.Insurance);
  calculation.add(AdditionReason.PremiumTheFollowingYear, damagePremium);
  return calculation;
}

export function damagePaymentByYourself(
  insurance: IInsurance,
  premium: number,
  currentLevel: number,
  damage: number
): Calculation {
  const insuranceObject = new Insurance(insurance);

  const premiumNextYear = insuranceObject.premiumInNYears(
    premium,
    currentLevel,
    1
  );

  const calculation = new Calculation(DamagePayedBy.Yourself);
  calculation.add(AdditionReason.Damage, damage);
  calculation.add(AdditionReason.PremiumTheFollowingYear, premiumNextYear);
  return calculation;
}

export function calculateCheaperOption(
  insurance: IInsurance,
  premium: number,
  currentLevel: number,
  damage: number
): ICalculation[] {
  const damagePayedByInsurance = damagePaymentByInsurance(
    insurance,
    premium,
    currentLevel
  );
  const damagePayedByYourself = damagePaymentByYourself(
    insurance,
    premium,
    currentLevel,
    damage
  );

  if (damagePayedByInsurance.total !== damagePayedByYourself.total) {
    return [damagePayedByInsurance, damagePayedByYourself];
  }

  const insuranceObject = new Insurance(insurance);

  const insuranceIn2Years = insuranceObject.premiumInNYears(
    premium,
    currentLevel,
    2
  );

  damagePayedByYourself.add(AdditionReason.PremiumIn2Years, insuranceIn2Years);

  const insuranceIn2YearsWithMalus = insuranceObject.premiumInNYears(
    premium,
    currentLevel,
    -insurance.levelIncrement + 1
  );
  damagePayedByInsurance.add(
    AdditionReason.PremiumIn2Years,
    insuranceIn2YearsWithMalus
  );
  return [damagePayedByInsurance, damagePayedByYourself];
}
