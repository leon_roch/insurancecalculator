import { createContext, useContext } from "react";
import { IChildren } from "../types/Children";
import { IInsurance } from "../types/Insurance";
import { insuranceCollection, storage } from "../configs/FirebaseConfig";
import { addDoc, getDoc, getDocs, doc } from "firebase/firestore";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
// interface that defines the data that is stored in the context
// it has functions to retrieve and store data in firestore and firebase storage
interface IStorageContext {
  getInsurances: () => Promise<IInsurance[]>;
  getInsurance: (id: string) => Promise<IInsurance>;
  addInsurance: (insurance: IInsurance) => Promise<void>;
  getImage: (path: string) => Promise<string>;
  uploadInsuranceImage: (file: File) => Promise<string>;
}

// context for storing data in firestore and firebase storage
// this context is used to store the user's and non user's data in firestore and firebase storage
// the data is stored in the context so that it can be accessed from any component
const storageContext = createContext<IStorageContext>({
  getInsurances: async () => [],
  getInsurance: async (id: string) => ({} as IInsurance),
  addInsurance: async (insurance: IInsurance) => {},
  getImage: async (path: string) => "",
  uploadInsuranceImage: async (file: File) => "",
});

// this context provider is exported and used by the App component
export function StorageProvider({ children }: IChildren) {
  const storageData = useStorageProvider();

  return (
    <storageContext.Provider value={storageData}>
      {children}
    </storageContext.Provider>
  );
}

// this hook is stores all the data in the context
function useStorageProvider(): IStorageContext {
  // function to retrieve all insurances from firestore
  // throws an error if the document does not exist
  const getInsurances = async () => {
    const querySnapshot = await getDocs(insuranceCollection);
    if (querySnapshot.empty) return [];
    return querySnapshot.docs.map((doc) => {
      if (!doc.exists()) throw new Error("No such document!");
      const insurance = doc.data();
      insurance.id = doc.id;
      return insurance as IInsurance;
    });
  };

  const getInsurance = async (id: string) => {
    const docRef = await getDoc(doc(insuranceCollection, id));
    if (!docRef.exists()) throw new Error("No such document!");
    const insurance = docRef.data();
    insurance.id = docRef.id;
    return insurance as IInsurance;
  };

  // function to add an insurance to firestore
  const addInsurance = async (insurance: IInsurance) => {
    const { id: _, ...withoutId } = insurance;
    await addDoc(insuranceCollection, withoutId);
  };

  const getImage = async (path: string): Promise<string> => {
    const imageReference = ref(storage, path);
    if (imageReference.fullPath === "") throw new Error("No such image!");
    return getDownloadURL(imageReference);
  };

  const uploadImage = async (file: File) => {
    const imageReference = ref(storage, "/insurances/" + file.name);
    await uploadBytes(imageReference, file);
    return imageReference.fullPath;
  };

  return {
    getInsurances: getInsurances,
    getInsurance: getInsurance,
    addInsurance: addInsurance,
    getImage: getImage,
    uploadInsuranceImage: uploadImage,
  };
}

// this hook is used to access the data in the context
export const useStorage = () => useContext(storageContext);
