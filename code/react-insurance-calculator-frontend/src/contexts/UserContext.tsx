import { User, getAuth, onAuthStateChanged } from "firebase/auth";
import { IChildren } from "../types/Children";
import { createContext, useContext, useEffect, useState } from "react";

interface IUserContext {
  user: User | null;
  isAdmin: boolean;
  contextInitiated: boolean;
}

const userContext = createContext<IUserContext>({
  user: null,
  isAdmin: false,
  contextInitiated: false,
});

export function UserProvider({ children }: IChildren) {
  const storage = useUserProvider();

  return (
    <userContext.Provider value={storage}>{children}</userContext.Provider>
  );
}

function useUserProvider(): IUserContext {
  const auth = getAuth();
  const [user, setUser] = useState<User | null>(null);
  const [contextInitiated, setContextInitiated] = useState(false);
  const adminUiDs = [
    "vVBwlvypuAWpB7BcCTrwnLqSVMP2", // leonardroch44@gmail.com
  ];
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setUser(user);
      setContextInitiated(true);
    });

    return unsubscribe;
  }, []);

  return {
    user: user,
    isAdmin: adminUiDs.includes(user?.uid ?? ""),
    contextInitiated: contextInitiated,
  };
}

export const useUser = () => useContext(userContext);
