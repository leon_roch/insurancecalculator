import { IChildren } from "../types/Children";
import { createContext, useContext, useEffect, useState } from "react";
import { Language } from "../types/Languages";

interface ILanguageContext {
  defaultLanguage?: Language;
  currentLanguage: Language;
  changeLanguage: (language: Language) => void;
  fallbackLanguage: Language;
}

interface ILanguageProviderProps {
  defaultLanguage?: Language;
  fallbackLanguage: Language;
}

const languageContext = createContext<ILanguageContext>({
  currentLanguage: Language.ENGLISH,
  changeLanguage: (language: string) => {},
  fallbackLanguage: Language.ENGLISH,
});

export function LanguageProvider({
  children,
  ...props
}: ILanguageProviderProps & IChildren) {
  const storage = useLanguageProvider(props);

  return (
    <languageContext.Provider value={storage}>
      {children}
    </languageContext.Provider>
  );
}

function useLanguageProvider({
  defaultLanguage,
  fallbackLanguage,
}: ILanguageProviderProps): ILanguageContext {
  const [currentLanguage, setCurrentLanguage] = useState(
    defaultLanguage ?? fallbackLanguage
  );

  useEffect(() => {
    if (!defaultLanguage) return;
    setCurrentLanguage(defaultLanguage);
  }, [defaultLanguage]);

  return {
    currentLanguage: currentLanguage,
    defaultLanguage: defaultLanguage,
    fallbackLanguage: fallbackLanguage,
    changeLanguage: setCurrentLanguage,
  };
}

export const useLanguage = () => useContext(languageContext);

export interface ITranslation {
  [key: string]: string;
}
