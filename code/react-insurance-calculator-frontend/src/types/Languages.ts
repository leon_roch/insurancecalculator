export enum Language {
  ENGLISH = "en",
  GERMAN = "de",
}
