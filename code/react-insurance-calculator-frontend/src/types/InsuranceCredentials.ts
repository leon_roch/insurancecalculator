export interface IInsuranceCredentials {
  insuranceId: string;
  insuranceLevel: number;
  insurancePrime: number;
  damage?: number;
}

export function instanceOfIInsuranceCredentials(
  object: any
): object is IInsuranceCredentials {
  return (
    object !== null &&
    "insuranceId" in object &&
    "insuranceLevel" in object &&
    "insurancePrime" in object
  );
}
