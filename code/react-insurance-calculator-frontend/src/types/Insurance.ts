// interface for the insurance object
export interface IInsurance {
  id: string;
  name: string;
  image: string;
  system: IInsuranceSystemLevel;
  levelIncrement: number;
}

export interface IInsuranceSystemLevel {
  [level: number]: number;
}

export class Insurance implements IInsurance {
  id: string;
  name: string;
  image: string;
  system: IInsuranceSystemLevel;
  levelIncrement: number;

  constructor(insurance: IInsurance) {
    this.id = insurance.id;
    this.name = insurance.name;
    this.image = insurance.image;
    this.system = insurance.system;
    this.levelIncrement = insurance.levelIncrement;
  }

  public defaultPremium(premium: number, currentLevel: number): number {
    return (premium / this.getSystemPercentage(currentLevel)) * 100;
  }

  public damagePremium(premium: number, currentLevel: number): number {
    return (
      (this.getSystemPercentage(currentLevel + this.levelIncrement) *
        this.defaultPremium(premium, currentLevel)) /
      100
    );
  }

  public premiumInNYears(
    premium: number,
    currentLevel: number,
    n: number = 1
  ): number {
    return (
      (this.defaultPremium(premium, currentLevel) *
        this.getSystemPercentage(currentLevel - n)) /
      100
    );
  }

  // method that returns the percentage of the system for a given level
  // if the level is not found, it returns the percentage of the smallest or largest level
  public getSystemPercentage(level: number): number {
    const percentage = this.system[level];
    if (percentage) return percentage;

    const levels = Object.keys(this.system).sort(
      (a, b) => parseInt(a) - parseInt(b)
    );
    const smallestLevel = levels[0];
    const largestLevel = levels[levels.length - 1];

    const clampedLevel = Math.min(
      Math.max(level, parseInt(smallestLevel)),
      parseInt(largestLevel)
    );
    return this.system[clampedLevel];
  }
}
