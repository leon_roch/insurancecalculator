export interface ILoginState {
  redirectPath?: string;
  state?: any;
}

export function instanceOfILoginState(object: any): object is ILoginState {
  return object !== null;
}
