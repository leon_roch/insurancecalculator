import { DamagePayedBy } from "./DamagePayedBy";

export interface ICalculation {
  additions: IAddition[];
  total: number;
  damagePayedBy: DamagePayedBy;
}

interface IAddition {
  name: AdditionReason;
  value: number;
  tooltip?: string;
}

export enum AdditionReason {
  Damage,
  PremiumTheFollowingYear,
  PremiumIn2Years,
}

export class Calculation implements ICalculation {
  additions: IAddition[];
  damagePayedBy: DamagePayedBy;

  constructor(damagePayedBy: DamagePayedBy, additions?: IAddition[]) {
    this.damagePayedBy = damagePayedBy;
    this.additions = additions ?? [];
  }

  public get total(): number {
    return this.additions
      .map((addition) => addition.value)
      .reduce((a, b) => a + b, 0);
  }

  public add(name: AdditionReason, value: number, tooltip?: string) {
    this.additions.push({ name: name, value: value, tooltip: tooltip });
  }
}
