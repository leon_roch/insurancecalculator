export interface IChildren {
  children?: JSX.Element[] | JSX.Element | string;
}
