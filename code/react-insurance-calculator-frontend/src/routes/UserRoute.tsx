import { useEffect } from "react";
import { useUser } from "../contexts/UserContext";
import { IChildren } from "../types/Children";
import { useLocation, useNavigate } from "react-router-dom";
import { ILoginState } from "../types/LoginPageState";

export function UserRoute({ children }: IChildren) {
  const { user, contextInitiated } = useUser();
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    // wait for context to be initiated
    if (!contextInitiated) return;
    if (user) return;
    navigate("/login", {
      state: { redirectPath: location.pathname } as ILoginState,
    });
    return;
  }, [contextInitiated, user]);
  return <>{children}</>;
}
