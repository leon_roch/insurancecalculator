import { useEffect } from "react";
import { useUser } from "../contexts/UserContext";
import { IChildren } from "../types/Children";
import { useLocation, useNavigate } from "react-router-dom";
import { ILoginState } from "./../types/LoginPageState";

export function AdminRoute({ children }: IChildren) {
  const { user, isAdmin, contextInitiated } = useUser();
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    // wait for context to be initiated
    if (!contextInitiated) return;
    if (!user) {
      navigate("/login", {
        state: { redirectPath: location.pathname } as ILoginState,
      });
      return;
    }
    if (isAdmin) return;
    // user without admin rights
    navigate("/");
  }, [contextInitiated, user]);

  return <>{user && children}</>;
}
