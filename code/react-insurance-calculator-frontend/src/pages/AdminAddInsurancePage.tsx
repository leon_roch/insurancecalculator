import {
  Box,
  Button,
  ButtonProps,
  Container,
  Flex,
  Image,
  Input,
  Skeleton,
  Text,
  VStack,
  useToast,
} from "@chakra-ui/react";
import { MyFormControl } from "../components/MyFormControl";
import { PageTitle } from "../layouts/PageTitle";
import { useEffect, useState } from "react";
import { IInsurance, IInsuranceSystemLevel } from "../types/Insurance";
import { useStorage } from "../contexts/StorageContext";
import { MySystemInput } from "../components/MySystemInput";

const InsuranceCard = ({
  insurance,
  ...props
}: { insurance: IInsurance } & ButtonProps) => {
  const [isSelected, setIsSelected] = useState(false);

  return (
    <Button
      width={"100%"}
      height={"100%"}
      {...props}
      variant={isSelected ? "solid" : "outline"}
      onClick={() => setIsSelected(!isSelected)}
      paddingBottom={"100%"}
      position={"relative"}
      boxShadow={"md"}
    >
      <Skeleton
        isLoaded={insurance.image !== ""}
        position={"absolute"}
        top={"5%"}
        bottom={0}
        left={"5%"}
        right={0}
        width={"90%"}
        height={"90%"}
      >
        <Image
          position={"absolute"}
          top={"5%"}
          bottom={0}
          left={"5%"}
          right={0}
          width={"90%"}
          height={"90%"}
          rounded={"base"}
          src={insurance.image}
          alt={insurance.name}
        />
        <Container
          css={{ ":hover": { opacity: 0.5 } }}
          transition={".4s ease"}
          opacity={0}
          position={"absolute"}
          top={"5%"}
          bottom={0}
          left={0}
          right={0}
          width={"90%"}
          height={"90%"}
          bg={"white"}
          rounded={"base"}
          blur={"md"}
        >
          <Text
            color={"black"}
            position={"absolute"}
            top={"50%"}
            left={"50%"}
            transform={"translate(-50%, -50%)"}
            textAlign={"center"}
          >
            {insurance.name}
          </Text>
        </Container>
      </Skeleton>
    </Button>
  );
};

export function AdminAddInsurancePage() {
  const [insuranceName, setInsuranceName] = useState("");
  const [image, setImage] = useState<File | null>(null);
  const [imagePreview, setImagePreview] = useState<string>("");
  const [fileReader, _] = useState(new FileReader());
  const [levelIncrement, setLevelIncrement] = useState(0);
  const [system, setSystem] = useState<IInsuranceSystemLevel>({});
  const [triedToSubmit, setTriedToSubmit] = useState(false);
  const { uploadInsuranceImage, addInsurance } = useStorage();
  const toast = useToast({
    position: "bottom",
    duration: 5000,
    isClosable: true,
    title: "Successfully created insurance",
    description: `The insurance ${insuranceName} was successfully created!`,
    status: "success",
  });

  useEffect(() => {
    fileReader.addEventListener("load", (e) => {
      setImagePreview(e.target?.result?.toString() ?? "");
    });
  }, []);

  const handleFileUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setImage(e.target.files[0]);
      fileReader.readAsDataURL(e.target.files[0]);
    }
  };

  const handleAddInsurance = () => {
    setTriedToSubmit(true);
    if (insuranceName === "" || image === null || levelIncrement === 0) return;
    uploadInsuranceImage(image)
      .then((url) => {
        addInsurance({
          id: "",
          name: insuranceName,
          image: url,
          levelIncrement: levelIncrement,
          system: system,
        })
          .then(() => toast())
          .catch((e) =>
            toast({ title: "Error", description: e.code, status: "error" })
          );
        toast({
          title: "Successfully uploaded image",
          description: `The image for the insurance ${insuranceName} was successfully uploaded!`,
        });
      })
      .catch((e) =>
        toast({ title: "Error", description: e.code, status: "error" })
      );
  };

  return (
    <>
      <PageTitle>Add Insurance</PageTitle>

      <Container minWidth={"100%"}>
        <Flex align={"center"} justify={"space-evenly"}>
          <VStack justify={"center"} spacing={7}>
            <MyFormControl
              label={"Insurance Name"}
              helperText="Please enter the name of the insurance"
              isRequired={true}
            >
              <Input
                value={insuranceName}
                onChange={(e) => setInsuranceName(e.target.value)}
                placeholder="Insurance Name"
              />
            </MyFormControl>
            <MyFormControl
              label={"Insurance Name"}
              helperText="Please enter the name of the insurance"
              isRequired={true}
            >
              <Input
                width={"100%"}
                height={"100%"}
                textAlign={"center"}
                type="file"
                onChange={handleFileUpload}
                accept="image/*"
              />
            </MyFormControl>
          </VStack>
          <Box height={"300px"} width={"300px"}>
            <InsuranceCard
              insurance={{
                id: "",
                image: imagePreview,
                name: insuranceName,
                system: system,
                levelIncrement: levelIncrement,
              }}
            />
          </Box>
        </Flex>
      </Container>

      <Container>
        <VStack justify={"center"} spacing={7}>
          <MyFormControl
            label={"Insurnace Level Increment"}
            helperText="Please enter the insurance level increment"
            isRequired={true}
          >
            <Input
              type="number"
              value={levelIncrement != 0 ? levelIncrement : ""}
              onChange={(e) => setLevelIncrement(parseInt(e.target.value))}
              placeholder="Insurance Level"
            />
          </MyFormControl>
          <Box width={"100%"}>
            <MyFormControl
              label={"System"}
              helperText="Please enter the system of the insurance"
              isRequired={true}
            >
              <MySystemInput system={(system) => setSystem(system)} />
            </MyFormControl>
            {Object.entries(system).map(([key, value]) => (
              <Text>
                {key}: {value}
              </Text>
            ))}
          </Box>

          <Button
            isDisabled={
              triedToSubmit &&
              (insuranceName === "" || image === null || levelIncrement === 0)
            }
            onClick={handleAddInsurance}
          >
            Add Insurance
          </Button>
        </VStack>
      </Container>
    </>
  );
}
