import { useEffect, useState } from "react";
import { InsuranceSelection } from "../components/InsuranceSelection";
import { useStorage } from "../contexts/StorageContext";
import { IInsurance } from "../types/Insurance";
import { InsuranceLevelSelector } from "../components/InsuranceLevelSelector";
import {
  Button,
  Center,
  Container,
  Flex,
  Heading,
  VStack,
} from "@chakra-ui/react";
import { useLocation, useNavigate } from "react-router-dom";
import {
  IInsuranceCredentials,
  instanceOfIInsuranceCredentials,
} from "../types/InsuranceCredentials";
import { MyNumberInput } from "./../components/MyNumberInput";
import { MyFormControl } from "../components/MyFormControl";
import { PageTitle } from "../layouts/PageTitle";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

// Todo: Error handling

export function InsurancePage() {
  const { getInsurances } = useStorage();
  const [insurances, setInsurances] = useState<IInsurance[]>([]);
  const [selectedInsurance, setSelectedInsurance] = useState<IInsurance | null>(
    null
  );
  const [error, setError] = useState<null | Error>(null);
  const [selectedLevel, setSelectedLevel] = useState<number | null>(null);
  const [primeInput, setPrimeInput] = useState("");
  const navigate = useNavigate();
  const state = useLocation().state as IInsuranceCredentials;
  const [triedToSubmit, setTriedToSubmit] = useState(false);

  // get all insurances from firestore
  const loadInsurances = () => {
    getInsurances()
      .then((insurances) => {
        if (insurances.length === 0) {
          setError(new Error("No insurances found"));
        }
        setInsurances(insurances);
      })
      .catch((err: Error) => {
        setError(err);
      });
  };

  // load insurances on mount
  useEffect(loadInsurances, []);

  // preselect insurance if passed as location state
  // prefill prime input if passed as location state
  useEffect(() => {
    if (insurances.length === 0 || !instanceOfIInsuranceCredentials(state))
      return;
    setPrimeInput(state.insurancePrime.toString());
    setSelectedInsurance(
      insurances.find((i) => i.id === state.insuranceId) ?? null
    );
  }, [insurances]);

  // preselect insurance level if passed as location state
  useEffect(() => {
    if (
      !state ||
      !selectedInsurance ||
      selectedInsurance.id != state.insuranceId
    )
      return;
    setSelectedLevel(state.insuranceLevel);
  }, [selectedInsurance]);

  // navigate to calculator page
  // pass the selected insurance and the selected insurance level to the calculator page
  const handleSubmit = () => {
    if (!selectedInsurance || !selectedLevel || primeInput == "") {
      setTriedToSubmit(true);
      return;
    }
    navigate("/calculator", {
      state: {
        ...state,
        insuranceId: selectedInsurance?.id,
        insuranceLevel: selectedLevel,
        insurancePrime: parseFloat(primeInput),
      } as IInsuranceCredentials,
    });
  };

  const pageTitle = useTranslation({
    [Language.ENGLISH]: "Insurance Credentials",
    [Language.GERMAN]: "Versicherungsdaten",
  });

  const insuranceHelperText = useTranslation({
    [Language.ENGLISH]: "Please select your insurance",
    [Language.GERMAN]: "Bitte wählen Sie Ihre Versicherung aus",
  });

  const continueText = useTranslation({
    [Language.ENGLISH]: "Continue",
    [Language.GERMAN]: "Weiter",
  });

  return (
    <>
      <PageTitle>{pageTitle}</PageTitle>
      <VStack spacing={7}>
        <Container>
          {error ? (
            <Center>
              <Heading>Something went wrong...</Heading>
            </Center>
          ) : (
            <MyFormControl
              isRequired
              isInvalid={triedToSubmit && !selectedInsurance}
              label={useTranslation({
                [Language.ENGLISH]: "Insurance Company",
                [Language.GERMAN]: "Versicherung",
              })}
              helperText={insuranceHelperText}
              errorText={insuranceHelperText}
            >
              <InsuranceSelection
                insurances={insurances}
                selectedInsurance={selectedInsurance}
                setSelectedInsurance={setSelectedInsurance}
              />
            </MyFormControl>
          )}
        </Container>
        <Container>
          <MyFormControl
            isRequired
            isInvalid={triedToSubmit && primeInput == ""}
            label={useTranslation({
              [Language.ENGLISH]: "Prime per Year",
              [Language.GERMAN]: "Prämie pro Jahr",
            })}
            helperText={useTranslation({
              [Language.ENGLISH]: "Please enter your insurance prime",
              [Language.GERMAN]: "Bitte geben Sie Ihre Versicherungsprämie ein",
            })}
            errorText={useTranslation({
              [Language.ENGLISH]: "Please enter your insurance prime",
              [Language.GERMAN]: "Bitte geben Sie Ihre Versicherungsprämie ein",
            })}
          >
            <MyNumberInput
              minValue={1}
              step={100}
              input={primeInput}
              setInput={setPrimeInput}
            />
          </MyFormControl>
        </Container>
        <Container>
          <MyFormControl
            isInvalid={triedToSubmit && !selectedLevel}
            isRequired
            label={useTranslation({
              [Language.ENGLISH]: "Insurance Level",
              [Language.GERMAN]: "Versicherungsstufe",
            })}
            helperText={useTranslation({
              [Language.ENGLISH]: "Please select your insurance level",
              [Language.GERMAN]: "Bitte wählen Sie Ihre Versicherungsstufe aus",
            })}
            errorText={
              selectedInsurance
                ? useTranslation({
                    [Language.ENGLISH]: "Please select your insurance level",
                    [Language.GERMAN]:
                      "Bitte wählen Sie Ihre Versicherungsstufe aus",
                  })
                : useTranslation({
                    [Language.ENGLISH]: "Please select your insurance first",
                    [Language.GERMAN]:
                      "Bitte wählen Sie zuerst Ihre Versicherung aus",
                  })
            }
          >
            <InsuranceLevelSelector
              options={selectedInsurance?.system ?? {}}
              selectedLevel={selectedLevel}
              setSelectedLevel={setSelectedLevel}
            />
          </MyFormControl>
        </Container>
        <Container>
          <Flex justify={"end"}>
            <Button
              width={"100%"}
              isDisabled={
                triedToSubmit &&
                (!selectedInsurance || !selectedLevel || primeInput == "")
              }
              onClick={handleSubmit}
            >
              {continueText}
            </Button>
          </Flex>
        </Container>
      </VStack>
    </>
  );
}
