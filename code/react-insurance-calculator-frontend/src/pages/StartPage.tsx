import {
  Button,
  Container,
  Heading,
  VStack,
  Image,
  Text,
  Link,
  useColorMode,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import CarCrashSmall from "../assets/car-crash-400W.jpg";
import CarCrashLarge from "../assets/car-crash.jpg";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

export function StartPage() {
  const navigate = useNavigate();

  const handleGetStarted = () => {
    navigate("/calculator");
  };

  const { colorMode } = useColorMode();

  const title = useTranslation({
    [Language.ENGLISH]: "To Pay or Not to Pay",
    [Language.GERMAN]: "Zahlen oder Nicht zahlen",
  });

  const titleSizeSM = useTranslation({
    [Language.ENGLISH]: "xl",
    [Language.GERMAN]: "md",
  });

  const description = useTranslation({
    [Language.ENGLISH]:
      "Our Car Damage Calculator Helps " + "to Make the Right Decision",
    [Language.GERMAN]:
      "Unser Kfz-Schadensrechner hilft, die richtige Entscheidung zu treffen",
  });

  const getStarted = useTranslation({
    [Language.ENGLISH]: "Get Started",
    [Language.GERMAN]: "Los legen",
  });

  const imageDescription = useTranslation({
    [Language.ENGLISH]: "Image by fxquadro on Freepik",
    [Language.GERMAN]: "Bild von fxquadro auf Freepik",
  });

  return (
    <>
      <Container maxWidth={"5xl"}>
        <VStack
          justify={"center"}
          paddingTop={{ base: 10, md: 35, lg: 100 }}
          paddingBottom={{ base: 0, md: 50 }}
          spacing={10}
        >
          <VStack spacing={5}>
            <Heading
              size={{ base: titleSizeSM, md: "2xl", lg: "3xl" }}
              textTransform={"uppercase"}
              color={colorMode === "light" ? "primary.800" : "primary.200"}
            >
              {title}
            </Heading>
            <Heading
              textAlign={"center"}
              size={{ base: "md", md: "md", lg: "lg" }}
            >
              {description}
            </Heading>
          </VStack>
          <Button size={"lg"} onClick={handleGetStarted}>
            {getStarted}
          </Button>
          <VStack>
            <Image
              width={"100%"}
              rounded={"base"}
              src={CarCrashLarge}
              fallbackSrc={CarCrashSmall}
            />
            <Text fontStyle={"italic"}>
              <Link href="https://www.freepik.com/free-photo/image-auto-accident-involving-two-cars_24471414.htm#query=car%20damage&position=0&from_view=search&track=ais">
                {imageDescription}
              </Link>
            </Text>
          </VStack>
        </VStack>
      </Container>
    </>
  );
}
