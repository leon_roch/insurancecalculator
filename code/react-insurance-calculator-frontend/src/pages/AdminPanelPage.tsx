import { useEffect, useState } from "react";
import { useStorage } from "../contexts/StorageContext";
import { PageTitle } from "../layouts/PageTitle";
import { IInsurance } from "../types/Insurance";
import { InsuranceSelection } from "../components/InsuranceSelection";
import { Button, Container, VStack } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export function AdminPanelPage() {
  const { getInsurances } = useStorage();
  const [insurances, setInsurances] = useState<IInsurance[]>([]);
  const navigate = useNavigate();

  useEffect(() => {
    loadInsurances();
  }, []);

  const loadInsurances = () => {
    getInsurances().then((insurances) => {
      setInsurances(insurances);
    });
  };

  return (
    <>
      <PageTitle>Admin Panel</PageTitle>
      <VStack width={"100%"} spacing={7}>
        <Container>
          <InsuranceSelection
            selectedInsurance={null}
            setSelectedInsurance={() => {}}
            insurances={insurances}
           />
        </Container>
        <Button
          bg={"primary.base"}
          onClick={() => {
            navigate("add");
          }}
        >
          Add Insurance
        </Button>
      </VStack>
    </>
  );
}
