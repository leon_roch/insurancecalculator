import { useLocation, useNavigate } from "react-router-dom";
import {
  IInsuranceCredentials,
  instanceOfIInsuranceCredentials,
} from "../types/InsuranceCredentials";
import { useEffect, useState } from "react";
import { Button, Center, Container, VStack } from "@chakra-ui/react";
import { useStorage } from "../contexts/StorageContext";
import { IInsurance } from "../types/Insurance";
import { calculateCheaperOption } from "../utils/InsuranceCalculatorUtils";
import { DamagePaymentOptionsView } from "../components/DamagePaymentOptionsView";
import { InsuranceCredentialsCard } from "../components/InsuranceCredentialsCard";
import { MyFormControl } from "../components/MyFormControl";
import { MyNumberInput } from "../components/MyNumberInput";
import { ICalculation } from "../types/Calculation";
import { PageTitle } from "../layouts/PageTitle";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

// component to calculate if damage should be paid by insurance
// or by the driver
// displays insurance information
// displays insurance level information
// displays insurance prime information
// displays input field for damage amount
// displays button to calculate
// displays result if damage should be paid by insurance or by driver
export function CalculatorPage() {
  const location = useLocation();
  const navigate = useNavigate();
  const { getInsurance } = useStorage();

  // states
  const [insurance, setInsurance] = useState<IInsurance | null>(null);
  const [insuranceCredentials, setInsuranceCredentials] =
    useState<IInsuranceCredentials | null>(null);
  const [damageInput, setDamageInput] = useState("");
  const [damagePaymentOptions, setDamagePaymentOptions] = useState<
    ICalculation[] | null
  >(null);
  const [triedToCalculate, setTriedToCalculate] = useState(false);

  const loadInsurance = (insuranceId: string) => {
    // TODO: error handling
    return getInsurance(insuranceId)
      .then((insurance) => {
        setInsurance(insurance);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    if (!instanceOfIInsuranceCredentials(location.state))
      return navigate("/insurance");
    setInsuranceCredentials(location.state);
    loadInsurance(location.state.insuranceId).then(() => {});
    if (location.state.damage !== undefined) {
      setDamageInput(location.state.damage.toString());
      // TODO: calculate
    }
  }, []);

  useEffect(() => {
    if (damageInput === "") return;
    location.state = {
      ...insuranceCredentials!,
      damage: parseFloat(damageInput),
    };
  }, [damageInput]);

  const handleChangeInsurance = () => {
    navigate("/insurance", { state: location.state });
  };

  const handleCalculate = () => {
    if (damageInput === "") {
      setTriedToCalculate(true);
      return;
    }
    calculate();
  };

  const calculate = () => {
    const damage = parseFloat(damageInput);
    const table = calculateCheaperOption(
      insurance!,
      location.state!.insurancePrime,
      location.state!.insuranceLevel,
      damage
    );
    setDamagePaymentOptions(table);
  };

  const titleText = useTranslation({
    [Language.ENGLISH]: "Calculator",
    [Language.GERMAN]: "Rechner",
  });

  const damageInputLabel = useTranslation({
    [Language.ENGLISH]: "Damage Amount",
    [Language.GERMAN]: "Schadenshöhe",
  });

  const damageInputHelperText = useTranslation({
    [Language.ENGLISH]: "Please enter the damage amount",
    [Language.GERMAN]: "Bitte geben Sie die Schadenshöhe ein",
  });
  const damageInputErrorText = useTranslation({
    [Language.ENGLISH]: "Please enter a valid the damage amount",
    [Language.GERMAN]: "Bitte geben Sie eine gültige Schadenshöhe ein",
  });
  const calculateButtonText = useTranslation({
    [Language.ENGLISH]: "Calculate",
    [Language.GERMAN]: "Berechnen",
  });

  return (
    <>
      <PageTitle>{titleText}</PageTitle>

      <VStack spacing={7}>
        <Container>
          <MyFormControl
            label={useTranslation({
              [Language.ENGLISH]: "Insurance Credentials",
              [Language.GERMAN]: "Versicherungsdaten",
            })}
            isRequired
            helperText={useTranslation({
              [Language.ENGLISH]:
                "To change the insurance credentials, please click the button",
              [Language.GERMAN]:
                "Um die Versicherungsdaten zu ändern, bitte auf den Button klicken",
            })}
          >
            <InsuranceCredentialsCard
              insuranceCredentials={insuranceCredentials ?? undefined}
              imagePath={insurance?.image}
              fallbackImage={"https://via.placeholder.com/1220"}
              handleOnChange={handleChangeInsurance}
            />
          </MyFormControl>
        </Container>
        <Container>
          <MyFormControl
            isRequired
            isInvalid={triedToCalculate && damageInput === ""}
            label={damageInputLabel}
            helperText={damageInputHelperText}
            errorText={damageInputErrorText}
          >
            <MyNumberInput
              input={damageInput}
              setInput={setDamageInput}
              minValue={1}
              step={100}
            />
          </MyFormControl>
        </Container>
        <Container>
          <Button
            width={"100%"}
            isDisabled={triedToCalculate && damageInput === ""}
            onClick={handleCalculate}
          >
            {calculateButtonText}
          </Button>
        </Container>
        <Center flexDirection={"column"}>
          <DamagePaymentOptionsView table={damagePaymentOptions ?? undefined} />
        </Center>
      </VStack>
    </>
  );
}
