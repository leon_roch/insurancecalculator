import { PageTitle } from "./../layouts/PageTitle";

import { EmailLoginForm } from "../components/EmailLoginForm";
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect, useRef } from "react";
import { instanceOfILoginState } from "../types/LoginPageState";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

export function LoginPage() {
  const navigate = useNavigate();
  const location = useLocation();
  const redirectPath = useRef<string | undefined>(undefined);
  const state = useRef<any>(undefined);

  useEffect(() => {
    if (!location.state || !instanceOfILoginState(location.state)) return;
    redirectPath.current = location.state.redirectPath;
    state.current = location.state.state;
  }, []);

  const handleLogin = () => {
    if (!redirectPath.current) return;
    navigate(redirectPath.current, { state: state.current ?? undefined });
  };

  const titleText = useTranslation({
    [Language.ENGLISH]: "Login",
    [Language.GERMAN]: "Anmelden",
  });

  return (
    <>
      <PageTitle>{titleText}</PageTitle>
      <EmailLoginForm onLogin={handleLogin} />
    </>
  );
}
