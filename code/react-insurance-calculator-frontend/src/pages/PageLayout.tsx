import { chakra } from "@chakra-ui/react";
import { Outlet } from "react-router-dom";
import { Footer } from "../layouts/Footer";
import { Header } from "../layouts/Header";
import "../styles/RemoveScrollBar.css"; // removes scrollbar on desktop browsers
import { useEffect } from "react";

export function PageLayout() {
  // scroll to top if not on top
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <>
      <chakra.div
        position={"relative"}
        minHeight={"100vh"}
        width={"100%"}
        marginBottom={{ base: 5, md: 8 }}
      >
        <Header />
        <Outlet />
      </chakra.div>
      <Footer />
    </>
  );
}
