import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
} from "@chakra-ui/react";

interface IMyFormControlProps {
  label?: string;
  helperText?: string;
  errorText?: string;
  isRequired?: boolean;
  isInvalid?: boolean;
  children?:
    | React.ReactNode
    | React.ReactNode[]
    | string
    | React.ReactElement
    | React.ReactElement[];
}

export function MyFormControl({
  label,
  helperText,
  errorText,
  isRequired,
  isInvalid,
  children,
}: IMyFormControlProps) {
  return (
    <FormControl isRequired={isRequired} isInvalid={isInvalid}>
      <FormLabel>{label}</FormLabel>
      {children}
      {isInvalid ? (
        <FormErrorMessage>{errorText}</FormErrorMessage>
      ) : (
        <FormHelperText>{helperText}</FormHelperText>
      )}
    </FormControl>
  );
}
