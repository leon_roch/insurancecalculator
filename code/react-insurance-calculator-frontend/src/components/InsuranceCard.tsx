import {
  Button,
  Container,
  Image,
  Skeleton,
  Text,
  useColorMode,
} from "@chakra-ui/react";
import { IInsurance } from "../types/Insurance";
import { useStorage } from "../contexts/StorageContext";
import { useEffect, useState } from "react";

interface InsuranceCardProps {
  insurance: IInsurance;
  onSelect: (insurance: IInsurance) => void;
  isSelected: boolean;
  fallbackImage?: string;
}

export function InsuranceCard({
  insurance,
  onSelect,
  isSelected,
  fallbackImage = "",
}: InsuranceCardProps) {
  const { getImage } = useStorage();
  const [image, setImage] = useState("");
  const { colorMode } = useColorMode();

  useEffect(() => {
    getImage(insurance.image)
      .then((url) => setImage(url))
      .catch(() => setImage(fallbackImage));
  }, []);

  return (
    <>
      <Button
        bg={
          isSelected
            ? colorMode === "light"
              ? "primary.50"
              : "primary.200"
            : colorMode === "light"
            ? "white"
            : "gray.800"
        }
        borderColor={colorMode === "light" ? "primary.50" : "primary.200"}
        _focus={{ bg: colorMode === "light" ? "primary.50" : "primary.200" }}
        _hover={{ bg: colorMode === "light" ? "primary.50" : "primary.200" }}
        variant={isSelected ? "solid" : "outline"}
        onClick={() => onSelect(insurance)}
        paddingBottom={"100%"}
        position={"relative"}
        boxShadow={"md"}
      >
        <Skeleton
          isLoaded={image !== ""}
          position={"absolute"}
          top={"5%"}
          bottom={0}
          left={"5%"}
          right={0}
          width={"90%"}
          height={"90%"}
        >
          <Image
            position={"absolute"}
            top={"5%"}
            bottom={0}
            left={"5%"}
            right={0}
            width={"90%"}
            height={"90%"}
            rounded={"base"}
            src={image}
            alt={insurance.name}
          />
          <Container
            css={{ ":hover": { opacity: 0.5 } }}
            transition={".4s ease"}
            opacity={0}
            position={"absolute"}
            top={"5%"}
            bottom={0}
            left={0}
            right={0}
            width={"90%"}
            height={"90%"}
            bg={"white"}
            rounded={"base"}
            blur={"md"}
          >
            <Text
              color={"black"}
              position={"absolute"}
              top={"50%"}
              left={"50%"}
              transform={"translate(-50%, -50%)"}
              textAlign={"center"}
            >
              {insurance.name}
            </Text>
          </Container>
        </Skeleton>
      </Button>
    </>
  );
}
