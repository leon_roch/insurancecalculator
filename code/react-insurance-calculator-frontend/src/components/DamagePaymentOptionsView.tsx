import {
  Text,
  TableContainer,
  Table,
  Tr,
  Center,
  Tbody,
  Td,
  Box,
  VStack,
  Stack,
  Heading,
  cssVar,
  Container,
  useColorMode,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { AdditionReason, ICalculation } from "../types/Calculation";
import { DamagePayedBy } from "../types/DamagePayedBy";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

interface IDamagePaymentOptionsViewProps {
  table?: ICalculation[];
}

const formattedNumber = (
  number: number,
  alwaysDisplayDecimals: boolean = false
) => {
  const rounded = Math.round(number * 20) / 20;
  if (Number.isInteger(rounded) && !alwaysDisplayDecimals)
    return rounded.toFixed(0);
  return rounded.toFixed(2);
};

export function DamagePaymentOptionsView({
  table,
}: IDamagePaymentOptionsViewProps) {
  const [cheaperOption, setCheaperOption] = useState<ICalculation | null>(null);
  const { colorMode } = useColorMode();

  useEffect(() => {
    setCheaperOption(table?.sort((a, b) => a.total - b.total)[0] ?? null);
  }, [table]);

  const insuranceCheaperOptionText = useTranslation({
    [Language.ENGLISH]:
      "If you go with the insurance, the premium for the next years will be cheaper than paying the damage yourself.",
    [Language.GERMAN]:
      "Wenn Sie sich für die Versicherung entscheiden, wird die Prämie für die nächsten Jahre günstiger sein als wenn Sie den Schaden selbst bezahlen.",
  });
  const YourselfCheaperOptionText = useTranslation({
    [Language.ENGLISH]:
      "If you pay the damage yourself, the premium for the next years will be cheaper than going with the insurance.",
    [Language.GERMAN]:
      "Wenn Sie den Schaden selbst bezahlen, wird die Prämie für die nächsten Jahre günstiger sein als wenn Sie sich für die Versicherung entscheiden.",
  });
  const cheaperOptionText = useTranslation({
    [Language.ENGLISH]: "Cheaper Option",
    [Language.GERMAN]: "Günstigere Option",
  });

  const DamagePayedByText = {
    [DamagePayedBy.Insurance]: useTranslation({
      [Language.ENGLISH]: "insurance",
      [Language.GERMAN]: "Versicherung",
    }),
    [DamagePayedBy.Yourself]: useTranslation({
      [Language.ENGLISH]: "yourself",
      [Language.GERMAN]: "selbst",
    }),
  };

  const AdditionReasonText = {
    [AdditionReason.Damage]: useTranslation({
      [Language.ENGLISH]: "Damage",
      [Language.GERMAN]: "Schaden",
    }),
    [AdditionReason.PremiumTheFollowingYear]: useTranslation({
      [Language.ENGLISH]: "Premium the following year",
      [Language.GERMAN]: "Prämie im Folgejahr",
    }),
    [AdditionReason.PremiumIn2Years]: useTranslation({
      [Language.ENGLISH]: "Premium in 2 years",
      [Language.GERMAN]: "Prämie in 2 Jahren",
    }),
  };

  return (
    <>
      {table && cheaperOption && (
        <>
          <VStack spacing={7}>
            <Container>
              <Text size={"md"}>
                {cheaperOption.damagePayedBy === DamagePayedBy.Insurance
                  ? insuranceCheaperOptionText
                  : YourselfCheaperOptionText}
              </Text>
            </Container>
            <Stack
              width={"100%"}
              spacing={{ base: 9, md: 5 }}
              direction={{ base: "column", md: "row" }}
              paddingX={"3%"}
            >
              {table
                .sort((a, b) => b.damagePayedBy - a.damagePayedBy)
                .map((calculation, indexTable) => (
                  <Box
                    position={"relative"}
                    paddingTop={5}
                    paddingX={5}
                    paddingBottom={3}
                    shadow={"lg"}
                    width={"100%"}
                    height={{ base: "100%", md: "initial" }}
                    rounded={"base"}
                    key={indexTable}
                    border={
                      cheaperOption === calculation
                        ? "2px solid " +
                          cssVar("chakra-colors-specialGreen-500").reference
                        : "1px solid " +
                          cssVar("chakra-colors-gray-500").reference
                    }
                  >
                    <VStack spacing={4}>
                      <VStack justify={"center"}>
                        <Heading size={"lg"} textTransform={"capitalize"}>
                          {DamagePayedByText[calculation.damagePayedBy]}
                        </Heading>
                        <Text>
                          {formattedNumber(calculation.total) + " CHF"}
                        </Text>
                      </VStack>
                      <TableContainer>
                        <Table size={{ base: "md", lg: "lg" }}>
                          <Tbody>
                            {calculation.additions.map((addition, index) => (
                              <>
                                <Tr key={index}>
                                  <Td>{AdditionReasonText[addition.name]}</Td>
                                  <Td textAlign={"end"}>
                                    {formattedNumber(addition.value, true)}
                                  </Td>
                                </Tr>
                              </>
                            ))}
                          </Tbody>
                        </Table>
                      </TableContainer>
                    </VStack>
                    {cheaperOption === calculation && (
                      <Center
                        position="absolute"
                        top={0}
                        left={0}
                        width={"100%"}
                        transform={"translateY(-50%)"}
                      >
                        <Text
                          textTransform={"uppercase"}
                          fontWeight="700"
                          fontSize={"2xs"}
                          rounded={"full"}
                          color={"gray.900"}
                          bg="specialGreen.500"
                          width={"fit-content"}
                          paddingX={"3%"}
                          paddingY={"0.5%"}
                        >
                          {cheaperOptionText}
                        </Text>
                      </Center>
                    )}
                  </Box>
                ))}
            </Stack>
          </VStack>
        </>
      )}
    </>
  );
}
