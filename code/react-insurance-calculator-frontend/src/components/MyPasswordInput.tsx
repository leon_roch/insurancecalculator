import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import {
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";

interface IMyPasswordInputProps {
  input: string;
  setInput: (value: string) => void;
  placeholder?: string;
}

export function MyPasswordInput({
  input,
  setInput,
  placeholder,
}: IMyPasswordInputProps) {
  const [show, setShow] = useState(false);

  return (
    <InputGroup>
      <Input
        placeholder={placeholder}
        type={show ? "text" : "password"}
        value={input}
        onChange={(e) => setInput(e.target.value)}
      />
      <InputRightElement>
        <IconButton
          variant={"ghost"}
          aria-label={show ? "Hide password" : "Show password"}
          icon={show ? <ViewOffIcon /> : <ViewIcon />}
          onClick={() => setShow(!show)}
        >
          {show ? "Hide" : "Show"}
        </IconButton>
      </InputRightElement>
    </InputGroup>
  );
}
