import {  SimpleGrid, Skeleton } from "@chakra-ui/react";
import { IInsurance } from "../types/Insurance";
import { InsuranceCard } from "./InsuranceCard";

interface InsuranceSelectionProps {
  insurances: IInsurance[];
  selectedInsurance: IInsurance | null;
  setSelectedInsurance: (insurance: IInsurance) => void;
}

// component that renders a list of possible insurances as buttons
// the user can select one of them
// the selected insurance is passed to the parent component via the onSelectInsurance callback
// the selected insurance is also stored in the state of this component
export function InsuranceSelection({
  insurances,
  selectedInsurance,
  setSelectedInsurance,
}: InsuranceSelectionProps) {
  return (
    <SimpleGrid columns={{ sm: 3, md: 3, lg: 4 }} gap={3}>
      {insurances.length > 0 ? (
        <>
          {insurances.map((insurance) => (
            <InsuranceCard
              key={insurance.name}
              insurance={insurance}
              onSelect={() => setSelectedInsurance(insurance)}
              isSelected={insurance === selectedInsurance}
              fallbackImage="https://via.placeholder.com/1220"
            />
          ))}
        </>
      ) : (
        [...Array(6)].map((_, i) => <Skeleton key={i} boxSize={"100px"} />)
      )}
    </SimpleGrid>
  );
}
