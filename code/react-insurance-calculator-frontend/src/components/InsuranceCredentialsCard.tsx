import {
  Skeleton,
  Image,
  Text,
  VStack,
  HStack,
  Button,
  Spacer,
  useColorMode,
} from "@chakra-ui/react";
import { IInsuranceCredentials } from "../types/InsuranceCredentials";
import { useEffect, useState } from "react";
import { useStorage } from "../contexts/StorageContext";
import { EditIcon } from "@chakra-ui/icons";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

interface IInsuranceCredentialsCardProps {
  insuranceCredentials?: IInsuranceCredentials;
  imagePath?: string;
  fallbackImage?: string;
  handleOnChange?: () => void;
}

export function InsuranceCredentialsCard({
  insuranceCredentials,
  imagePath,
  fallbackImage = "",
  handleOnChange,
}: IInsuranceCredentialsCardProps) {
  const [image, setImage] = useState("");
  const { getImage } = useStorage();
  const { colorMode } = useColorMode();

  const loadImage = () => {
    getImage(imagePath!)
      .then((url) => setImage(url))
      .catch(() => {
        setImage(fallbackImage);
      });
  };

  useEffect(() => {
    if (imagePath === undefined) return;
    loadImage();
  }, [imagePath]);

  const primeText = useTranslation({
    [Language.ENGLISH]: "Prime: ",
    [Language.GERMAN]: "Prämie: ",
  });
  const levelText = useTranslation({
    [Language.ENGLISH]: "Level: ",
    [Language.GERMAN]: "Stufe: ",
  });
  const changeText = useTranslation({
    [Language.ENGLISH]: "Change",
    [Language.GERMAN]: "Ändern",
  });
  return (
    <>
      <Button
        width={"100%"}
        height={"fit-content"}
        padding={"10px"}
        variant={"outline"}
        onClick={handleOnChange}
        color={colorMode === "light" ? "black" : "white"}
        borderColor={colorMode === "light" ? "primary.50" : "primary.200"}
      >
        <HStack spacing={5} width={"100%"}>
          <Skeleton width={"50px"} height={"50px"} isLoaded={image !== ""}>
            <Image rounded={"base"} src={image} />
          </Skeleton>
          <VStack alignItems={"space-evenly"} justifyItems={"start"}>
            <HStack>
              <Text>{primeText}</Text>
              <Spacer />
              <Skeleton
                isLoaded={
                  insuranceCredentials !== undefined &&
                  insuranceCredentials.insurancePrime !== undefined
                }
              >
                <Text>{insuranceCredentials?.insurancePrime}</Text>
              </Skeleton>
            </HStack>
            <HStack>
              <Text>{levelText}</Text>
              <Spacer />
              <Skeleton isLoaded={insuranceCredentials !== undefined}>
                <Text>{insuranceCredentials?.insuranceLevel}</Text>
              </Skeleton>
            </HStack>
          </VStack>
          <Spacer />
          <HStack>
            <Text
              color={colorMode === "light" ? "primary.base" : "primary.200"}
            >
              {changeText}
            </Text>
            <EditIcon
              color={colorMode === "light" ? "primary.base" : "primary.200"}
            />
          </HStack>
        </HStack>
      </Button>
    </>
  );
}
