import {
  NumberInput,
  InputGroup,
  InputLeftAddon,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react";

interface IMyNumberInputProps {
  input: string;
  setInput: (value: string) => void;
  minValue?: number;
  maxValue?: number;
  step?: number;
}

export function MyNumberInput({
  input,
  setInput,
  minValue,
  maxValue,
  step,
}: IMyNumberInputProps) {
  return (
    <NumberInput
      colorScheme="primary"
      focusBorderColor="primary.500"
      errorBorderColor="red.base"
      keepWithinRange
      clampValueOnBlur
      defaultValue={""}
      allowMouseWheel
      value={input}
      min={minValue}
      max={maxValue}
      onChange={(value) => setInput(value)}
      step={step}
    >
      <InputGroup>
        <InputLeftAddon children="CHF" />
        <NumberInputField />
      </InputGroup>
      <NumberInputStepper>
        <NumberIncrementStepper />
        <NumberDecrementStepper />
      </NumberInputStepper>
    </NumberInput>
  );
}
