import { useLocation, useNavigate } from "react-router-dom";
import { useUser } from "../contexts/UserContext";
import {
  Avatar,
  Button,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  useToast,
} from "@chakra-ui/react";
import { getAuth, signOut } from "firebase/auth";
import { Language } from "../types/Languages";
import { useTranslation } from "../hooks/useTranslation";

export function HeaderUserIcon() {
  const { user, isAdmin } = useUser();
  const navigate = useNavigate();
  const location = useLocation();
  const toast = useToast({
    position: "bottom",
    duration: 5000,
    isClosable: true,
    title: useTranslation({
      [Language.ENGLISH]: "Successfully signed out",
      [Language.GERMAN]: "Erfolgreich abgemeldet",
    }),
    description: useTranslation({
      [Language.ENGLISH]: `See you soon!`,
      [Language.GERMAN]: `Bis bald!`,
    }),
    status: "success",
  });

  const handleLogin = () => {
    navigate("/login", {
      state: { redirectPath: location.pathname, state: location.state },
    });
  };

  const handLogout = () => {
    signOut(getAuth());
    toast();
  };

  const signInText = useTranslation({
    [Language.ENGLISH]: "Sign In",
    [Language.GERMAN]: "Anmelden",
  });

  const logoutText = useTranslation({
    [Language.ENGLISH]: "Log Out",
    [Language.GERMAN]: "Abmelden",
  });

  return (
    <>
      {user ? (
        <Menu autoSelect={false} size={"sm"}>
          <MenuButton>
            <Avatar
              name={user.displayName ?? user.email ?? ""}
              width={{ base: "32px", md: "40px" }}
              height={{ base: "32px", md: "40px" }}
              src={user.photoURL ?? ""}
            />
          </MenuButton>
          <MenuList>
            <MenuItem onClick={handLogout}>{logoutText}</MenuItem>
            {isAdmin && (
              <MenuItem
                onClick={() => {
                  navigate("/admin");
                }}
              >
                Go to Admin Panel
              </MenuItem>
            )}
          </MenuList>
        </Menu>
      ) : (
        <Button onClick={handleLogin}>{signInText}</Button>
      )}
    </>
  );
}
