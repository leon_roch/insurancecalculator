import { VStack, Button, Container, Input, useToast } from "@chakra-ui/react";
import {
  signInWithPopup,
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  fetchSignInMethodsForEmail,
  UserCredential,
} from "firebase/auth";
import { useState, useEffect } from "react";
import { GoogleProvider } from "../configs/FirebaseConfig";
import { MyFormControl } from "./MyFormControl";
import { MyPasswordInput } from "./MyPasswordInput";
import { FcGoogle } from "react-icons/fc";
import { MdOutlineEmail } from "react-icons/md";
import { useUser } from "../contexts/UserContext";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

interface IInvalidPassword {
  invalidPassword: string;
  reason?: string;
}

const eightNumbers: IInvalidPassword = {
  invalidPassword: "12345678",
  reason: "Password shouldn't be numbers 1 to 8",
};

const theWordPassword: IInvalidPassword = {
  invalidPassword: "password",
  reason: "Password shouldn't be the word 'password'",
};

const theWordPASSWORD: IInvalidPassword = {
  invalidPassword: "PASSWORD",
  reason: "Password shouldn't be the word 'PASSWORD'",
};

interface IEmailLoginFormProps {
  onLogin?: () => void;
}

export function EmailLoginForm({ onLogin = () => {} }: IEmailLoginFormProps) {
  const auth = getAuth();
  const { user } = useUser();
  const [authenticating, setAuthenticating] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [validPassword, setValidPassword] = useState(false);
  const [triedToLogin, setTriedToLogin] = useState(false);
  const [passwordErrorText, setPasswordErrorText] = useState(
    "Please enter a valid password"
  );
  const invalidPasswords: IInvalidPassword[] = [
    eightNumbers,
    theWordPassword,
    theWordPASSWORD,
    { invalidPassword: email, reason: "Password shouldn't be the email" },
    {
      invalidPassword: password.charAt(0).repeat(password.length),
      reason: "Password shouldn't be the same character repeated",
    },
  ];
  const toast = useToast({
    position: "bottom",
    duration: 5000,
    isClosable: true,
    title: useTranslation({
      [Language.ENGLISH]: "Successfully signed in",
      [Language.GERMAN]: "Erfolgreich angemeldet",
    }),
    description: useTranslation({
      [Language.ENGLISH]: `Welcome ${
        user?.displayName ?? user?.email ?? "user"
      }!`,
      [Language.GERMAN]: `Willkommen ${
        user?.displayName ?? user?.email ?? "Benutzer"
      }!`,
    }),
    status: "success",
  });

  useEffect(() => {
    const valid = validPasswordLength() && validPasswordContent();

    setValidPassword(valid);
  }, [password, email]);

  const validPasswordLength = () => {
    if (!(password.length >= 8 && password.length <= 128)) {
      setPasswordErrorText("Password must be between 8 and 128 characters");
      return false;
    }
    return true;
  };

  const validPasswordContent = () => {
    const invalidPassword = invalidPasswords.find(
      (invalidPassword) => invalidPassword.invalidPassword === password
    );
    if (!invalidPassword) return true;
    setPasswordErrorText(
      invalidPassword.reason ?? "Please enter a valid password"
    );
    console.log(invalidPassword);
    return false;
  };

  const emailIsValid = () => {
    return /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,}$/.test(email);
  };

  const handleEmailLogin = async () => {
    setTriedToLogin(true);
    if (!validPassword || !emailIsValid()) return;

    setAuthenticating(true);
    let methods: string[] = [];
    try {
      methods = await fetchSignInMethodsForEmail(auth, email);
    } catch (error) {
      toast({
        title: "Could not sign in",
        description: "Invalid email",
        status: "error",
      });
      setAuthenticating(false);
      return;
    }
    if (methods.includes("password")) {
      signInWithEmailAndPassword(auth, email, password)
        .then((credentials) => onLoginSuccess(credentials))
        .catch(() => {
          toast({
            title: "Could not sign in",
            description: "Invalid email or password",
            status: "error",
          });
        });
    } else if (methods.length === 0) {
      createUserWithEmailAndPassword(auth, email, password)
        .then((credentials) => onLoginSuccess(credentials))
        .catch((e) => {
          toast({
            title: "Could not sign up",
            description: e.code,
            status: "error",
          });
        });
    } else {
      toast({
        title: "Wrong login method",
        description:
          "Cannot sign in with email and password. Please use Google sign in.",
        status: "error",
      });
    }
    setAuthenticating(false);
  };

  const handleGoogleLogin = () => {
    setAuthenticating(true);
    signInWithPopup(auth, GoogleProvider)
      .then((credentials) => onLoginSuccess(credentials))
      .catch((error) => {
        if (error.code === "auth/popup-closed-by-user") return;
        toast({
          title: "Could not sign in",
          description:
            "An error occurred while signing in. Please try again later. " +
            error.code,
          status: "error",
        });
      })
      .finally(() => setAuthenticating(false));
  };

  const onLoginSuccess = (userCredentials: UserCredential) => {
    toast();
    onLogin();
  };

  const loginWithGoogleText = useTranslation({
    [Language.ENGLISH]: "Sign in with Google",
    [Language.GERMAN]: "Mit Google anmelden",
  });

  const loginWithEmailText = useTranslation({
    [Language.ENGLISH]: "Sign in with Email",
    [Language.GERMAN]: "Mit Email anmelden",
  });

  return (
    <>
      <VStack spacing={7} justify={"center"}>
        <Container>
          <MyFormControl
            isInvalid={triedToLogin && !emailIsValid()}
            label={useTranslation({
              [Language.ENGLISH]: "Email",
              [Language.GERMAN]: "E-Mail",
            })}
            helperText={useTranslation({
              [Language.ENGLISH]: "Please enter your email address",
              [Language.GERMAN]: "Bitte geben Sie Ihre E-Mail-Adresse ein",
            })}
            errorText={useTranslation({
              [Language.ENGLISH]: "Please enter your email address",
              [Language.GERMAN]: "Bitte geben Sie Ihre E-Mail-Adresse ein",
            })}
            isRequired
          >
            <Input
              type="email"
              placeholder={useTranslation({
                [Language.ENGLISH]: "Enter your email address",
                [Language.GERMAN]: "Geben Sie Ihre E-Mail-Adresse ein",
              })}
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </MyFormControl>
        </Container>
        <Container>
          <MyFormControl
            isRequired
            isInvalid={triedToLogin && !validPassword}
            label={useTranslation({
              [Language.ENGLISH]: "Password",
              [Language.GERMAN]: "Passwort",
            })}
            helperText={useTranslation({
              [Language.ENGLISH]: "Please enter your password",
              [Language.GERMAN]: "Bitte geben Sie Ihr Passwort ein",
            })}
            errorText={passwordErrorText}
          >
            <MyPasswordInput
              input={password}
              setInput={setPassword}
              placeholder={useTranslation({
                [Language.ENGLISH]: "Enter your password",
                [Language.GERMAN]: "Geben Sie Ihr Passwort ein",
              })}
            />
          </MyFormControl>
        </Container>
        <Container>
          <VStack>
            <Button
              leftIcon={<MdOutlineEmail />}
              width={"100%"}
              isDisabled={triedToLogin && (!validPassword || !emailIsValid())}
              isLoading={authenticating}
              onClick={handleEmailLogin}
              variant={"outline"}
            >
              {loginWithEmailText}
            </Button>
            <Button
              leftIcon={<FcGoogle />}
              width={"100%"}
              onClick={handleGoogleLogin}
              isLoading={authenticating}
            >
              {loginWithGoogleText}
            </Button>
          </VStack>
        </Container>
      </VStack>
    </>
  );
}
