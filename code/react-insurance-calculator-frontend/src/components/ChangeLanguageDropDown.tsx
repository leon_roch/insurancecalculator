import { Button, Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import { useLanguage } from "../contexts/LanguageContext";
import { Language } from "../types/Languages";

const languageIcon: { [key: string]: string } = {
  [Language.ENGLISH]: "English",
  [Language.GERMAN]: "Deutsch",
};

interface IChangeLanguageDropDownProps {}

export function ChangeLanguageDropDown(props: IChangeLanguageDropDownProps) {
  const { currentLanguage, changeLanguage } = useLanguage();

  const handleLanguageChange = (key: string, value: string) => {
    changeLanguage(value as Language);
  };
  return (
    <Menu autoSelect={false}>
      <MenuButton textAlign={"center"} as={Button} variant={"outline"}>
        {languageIcon[currentLanguage]}
      </MenuButton>
      <MenuList>
        {Object.entries(Language)
          .filter(([_, value]) => value !== currentLanguage)
          .map(([key, value]) => (
            <MenuItem
              key={value}
              onClick={() => handleLanguageChange(key, value)}
            >
              {languageIcon[value]}
            </MenuItem>
          ))}
      </MenuList>
    </Menu>
  );
}
