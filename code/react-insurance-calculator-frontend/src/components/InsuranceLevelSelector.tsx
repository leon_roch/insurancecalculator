import { useEffect, useRef } from "react";
import { IInsuranceSystemLevel } from "../types/Insurance";
import { Select } from "@chakra-ui/react";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

interface IInsuranceLevelSelectorProps {
  // all possible insurance levels
  // insurance level: percentage of insurance level
  options: IInsuranceSystemLevel;
  selectedLevel: number | null;
  setSelectedLevel: (value: number | null) => void;
}

// component that renders a select element with all possible insurance levels
// the user can select one of them
export function InsuranceLevelSelector({
  options,
  selectedLevel,
  setSelectedLevel,
}: IInsuranceLevelSelectorProps) {
  const selectRef = useRef<HTMLSelectElement>(null);

  // reset option if options changes
  useEffect(() => {
    if (selectRef.current) selectRef.current.selectedIndex = 0;
    setSelectedLevel(null);
  }, [options]);

  const handleOptionChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedLevel(parseInt(e.target.selectedOptions[0].text));
  };

  return (
    <label>
      <Select
        ref={selectRef}
        name="insurance levels"
        disabled={Object.keys(options).length == 0}
        onChange={handleOptionChange}
        value={options[selectedLevel ?? -100]}
      >
        <option hidden disabled value="">
          {useTranslation({
            [Language.ENGLISH]: "Choose insurance level",
            [Language.GERMAN]: "Versicherungsstufe wählen",
          })}
        </option>
        {Object.keys(options)
          .sort((a, b) => parseInt(a) - parseInt(b))
          .map((level, index) => (
            <option key={index} value={options[parseInt(level)]}>
              {level}
            </option>
          ))}
      </Select>
    </label>
  );
}
