import { Button, Container, Input, InputGroup, VStack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { IInsuranceSystemLevel } from "../types/Insurance";

interface ISystemInput {
  id: number;
  key: number | null;
  value: number | null;
}

interface IMySystemInputProps {
  system: (system: IInsuranceSystemLevel) => void;
}

export function MySystemInput({ system }: IMySystemInputProps) {
  const [systemInputs, setSystemInputs] = useState<ISystemInput[]>([]);

  const handleAddSystemInput = () => {
    setSystemInputs([
      ...systemInputs,
      {
        id: systemInputs.length,
        key:
          systemInputs.length > 0
            ? systemInputs[systemInputs.length - 1].key != null
              ? systemInputs[systemInputs.length - 1]!.key! + 1
              : null
            : null,
        value:
          systemInputs.length > 0
            ? systemInputs[systemInputs.length - 1].value != null
              ? systemInputs[systemInputs.length - 1]!.value! + 10
              : null
            : null,
      },
    ]);
  };

  const handleChangeSystemInput = (
    id: number,
    key: number | null,
    value: number | null
  ) => {
    setSystemInputs(
      systemInputs.map((systemInput) => {
        if (systemInput.id === id) {
          return { ...systemInput, key: key, value: value };
        }
        return systemInput;
      })
    );
  };

  const handleRemoveSystemInput = (id: number) => {
    setSystemInputs(
      systemInputs.filter((systemInput) => systemInput.id !== id)
    );
  };

  useEffect(() => {
    system(
      systemInputs.reduce((obj: IInsuranceSystemLevel, input) => {
        if (!input.key || !input.value) return obj;
        obj[input.key] = input.value;
        return obj;
      }, {})
    );
  }, [systemInputs]);

  return (
    <VStack spacing={4}>
      {systemInputs.map((systemInput, index) => (
        <InputGroup key={index}>
          <Input
            type="text"
            placeholder="key"
            value={systemInput.key ?? ""}
            onChange={(e) =>
              handleChangeSystemInput(
                systemInput.id,
                e.target.value != "" && e.target.value != "-"
                  ? parseInt(e.target.value)
                  : null,
                systemInput.value
              )
            }
          />
          <Input
            type="number"
            placeholder="value"
            value={systemInput.value ?? ""}
            onKeyDown={(e) => {
              if (e.key !== "Enter") return;
              handleAddSystemInput();
            }}
            onChange={(e) =>
              handleChangeSystemInput(
                systemInput.id,
                systemInput.key,
                parseFloat(e.target.value)
              )
            }
          />
          <Button onClick={() => handleRemoveSystemInput(systemInput.id)}>
            X
          </Button>
        </InputGroup>
      ))}
      <Button onClick={handleAddSystemInput}>Add</Button>
    </VStack>
  );
}
