import "./configs/FirebaseConfig";
import {
  Outlet,
  RouteObject,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";
import { InsurancePage } from "./pages/InsurancePage";
import { StorageProvider } from "./contexts/StorageContext";
import { CalculatorPage } from "./pages/CalculatorPage";
import { StartPage } from "./pages/StartPage";
import { PageLayout } from "./pages/PageLayout";
import { LoginPage } from "./pages/LoginPage";
import { UserProvider } from "./contexts/UserContext";
import { AdminRoute } from "./routes/AdminRoute";
import { AdminPanelPage } from "./pages/AdminPanelPage";
import { AdminAddInsurancePage } from "./pages/AdminAddInsurancePage";
import { LanguageProvider } from "./contexts/LanguageContext";
import { Language } from "./types/Languages";
import { useBrowserLanguage } from "./hooks/useBrowserLanguage";

const routes: RouteObject[] = [
  {
    path: "/",
    element: <PageLayout />,
    children: [
      {
        path: "/",
        element: <StartPage />,
      },
      {
        path: "/login",
        element: <LoginPage />,
      },
      {
        path: "/insurance",
        element: (
          <>
            <InsurancePage />
          </>
        ),
      },
      {
        path: "/calculator",
        element: (
          <>
            <CalculatorPage />
          </>
        ),
      },
      {
        path: "/admin",
        element: (
          <>
            <AdminRoute>
              <Outlet />
            </AdminRoute>
          </>
        ),
        children: [
          {
            path: "",
            element: <AdminPanelPage />,
          },
          {
            path: "add",
            element: <AdminAddInsurancePage />,
          },
        ],
      },
    ],
  },
];

const router = createBrowserRouter(routes);

function App() {
  return (
    <StorageProvider>
      <UserProvider>
        <LanguageProvider
          defaultLanguage={useBrowserLanguage()}
          fallbackLanguage={Language.ENGLISH}
        >
          <RouterProvider router={router} />
        </LanguageProvider>
      </UserProvider>
    </StorageProvider>
  );
}

export default App;
