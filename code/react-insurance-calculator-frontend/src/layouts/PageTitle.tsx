import { Center, Heading, HeadingProps } from "@chakra-ui/react";

interface IPageTitleProps {
  children?: string | React.ReactNode;
}

export function PageTitle({
  children,
  ...headingProps
}: IPageTitleProps & HeadingProps) {
  return (
    <Center height={"100px"}>
      <Heading textAlign={"center"} size={"2xl"} {...headingProps}>
        {children}
      </Heading>
    </Center>
  );
}
