import { CloseIcon, HamburgerIcon, SunIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  ButtonGroup,
  Flex,
  Heading,
  IconButton,
  Spacer,
  Link as ChakraLink,
  HStack,
  useDisclosure,
  VStack,
  ButtonProps,
  useColorMode,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { HeaderUserIcon } from "../components/HeaderUserIcon";
import { ChangeLanguageDropDown } from "../components/ChangeLanguageDropDown";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

interface INavLink {
  label: string;
  href: string;
}

export function Header() {
  const { isOpen, onOpen, onClose } = useDisclosure({ defaultIsOpen: false });
  const { colorMode, toggleColorMode } = useColorMode();

  const navLinks: INavLink[] = [
    {
      label: useTranslation({
        [Language.ENGLISH]: "Calculator",
        [Language.GERMAN]: "Rechner",
      }),
      href: "/calculator",
    },
  ];

  const NavLinkButton = ({ label, href, ...rest }: INavLink & ButtonProps) => (
    <Button
      {...rest}
      as={Link}
      to={href}
      variant={"ghost"}
      color={colorMode === "light" ? "primary.600" : "primary.50"}
      _hover={{ color: "primary.800", bg: "primary.50" }}
      onClick={() => onClose()}
    >
      {label}
    </Button>
  );

  const handleChangeColorMode = () => {
    toggleColorMode();
  };

  const logoTitle = useTranslation({
    [Language.ENGLISH]: "Car Damage Calculator",
    [Language.GERMAN]: "Kfz-Schadensrechner",
  });

  return (
    <>
      <Box
        position={"sticky"}
        top={0}
        zIndex={1}
        bg={colorMode === "light" ? "white" : "gray.700"}
        shadow={"base"}
        width={"100%"}
        paddingX={1}
        paddingY={2}
      >
        <Flex justify={"space-evenly"} height={"fit-content"} align={"center"}>
          <IconButton
            size={"sm"}
            aria-label="Open Menu"
            icon={isOpen ? <CloseIcon /> : <HamburgerIcon />}
            onClick={isOpen ? onClose : onOpen}
            display={{ md: "none" }}
            variant={"outline"}
          />

          <Flex
            paddingRight={{ base: 0, md: 6 }}
            flex={{ base: "max-content", md: "none" }}
            justify={{ base: "center", md: "start" }}
          >
            <ChakraLink
              marginLeft={"5px"}
              as={Link}
              to={"/"}
              onClick={() => onClose()}
            >
              <Heading
                size={{ base: "xs", md: "sm", lg: "md" }}
                color={colorMode === "light" ? "primary.800" : "primary.200"}
              >
                {logoTitle}
              </Heading>
            </ChakraLink>
          </Flex>
          <HStack spacing={5}>
            {navLinks.map((link, index) => (
              <NavLinkButton
                {...link}
                paddingY={2}
                height={"fit-content"}
                key={index}
                display={{ base: "none", md: "initial" }}
              />
            ))}
          </HStack>
          <Spacer display={{ base: "none", md: "initial" }} />
          <ButtonGroup
            spacing={{ base: 1, md: 3 }}
            size={{ base: "sm", md: "md" }}
          >
            <ChangeLanguageDropDown />
            <IconButton
              aria-label="Color Mode"
              icon={<SunIcon />}
              onClick={handleChangeColorMode}
              variant={"outline"}
            />
            <HeaderUserIcon />
          </ButtonGroup>
        </Flex>
        {isOpen && (
          <Box height={"100%"} width={"100%"} display={{ md: "none" }}>
            <VStack align={"start"}>
              {navLinks.map((link, index) => (
                <NavLinkButton {...link} key={index} />
              ))}
            </VStack>
          </Box>
        )}
      </Box>
    </>
  );
}
