import { Box, Flex, Text, useColorMode } from "@chakra-ui/react";
import { useTranslation } from "../hooks/useTranslation";
import { Language } from "../types/Languages";

export function Footer() {
  const { colorMode } = useColorMode();

  const text = useTranslation({
    [Language.ENGLISH]: "© 2023 Léonard Roch. All rights reserved",
    [Language.GERMAN]: "© 2023 Léonard Roch. Alle Rechte vorbehalten",
  });

  return (
    <Box
      position={"relative"}
      width={"100%"}
      bg={colorMode === "dark" ? "gray.900" : "gray.100"}
      paddingY={5}
    >
      <Flex width={"100%"} justify={"center"}>
        <Text>{text}</Text>
      </Flex>
    </Box>
  );
}
