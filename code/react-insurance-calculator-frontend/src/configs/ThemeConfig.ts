import { ThemeConfig, extendTheme } from "@chakra-ui/react";
import { Tokens } from "../../.mirrorful/theme";

const breakpoints = {
  sm: "320px",
  md: "768px",
  lg: "960px",
  xl: "1200px",
  "2xl": "1536px",
};

const colors = {
  primary: {
    ...Tokens.colors.blue,
  },
  red: {
    ...Tokens.colors["torch-red"],
  },
  specialGreen: {
    ...Tokens.colors["bright-green"],
  },
};

const components = {
  Button: {
    defaultProps: {
      colorScheme: "primary",
    },
  },

  Input: {
    defaultProps: {
      colorTheme: "primary",
      focusBorderColor: "primary.500",
      errorBorderColor: "red.base",
    },
  },
  NumberInputField: {
    defaultProps: {
      colorTheme: "primary",
      focusBorderColor: "primary.500",
      errorBorderColor: "red.base",
    },
  },
  FormErrorText: {
    defaultProps: {
      color: "red.base",
    },
  },
};

const config: ThemeConfig = {
  initialColorMode: "system",
  useSystemColorMode: true,
};

export const theme = extendTheme({
  breakpoints,
  colors,
  components,
  config,
});
