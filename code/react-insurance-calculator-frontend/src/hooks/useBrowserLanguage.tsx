import { useEffect, useState } from "react";
import { Language } from "../types/Languages";

export function useBrowserLanguage() {
  const [language, setLanguage] = useState<Language | undefined>(undefined);

  useEffect(() => {
    // I only need the language code, not the region code
    const browserLanguage = navigator.language.substring(0, 2);
    setLanguage(Object.values(Language).find((l) => l === browserLanguage));
  }, []);

  return language;
}
