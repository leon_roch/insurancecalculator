import { ITranslation, useLanguage } from "../contexts/LanguageContext";

/**
 *
 * @param translation languages
 * @returns text of the current language
 *
 * @example
 * const translation = {
 * [Language.ENGLISH]: "Hello",
 * de: "Hallo", // [Language.GERMAN] = "de"
 * }
 *
 * const text = useTranslation(translation);
 *
 * console.log(text); // Hello or Hallo
 */

export const useTranslation = (translation: ITranslation) => {
  const { currentLanguage, fallbackLanguage } = useLanguage();

  return translation[currentLanguage] ?? translation[fallbackLanguage];
};
