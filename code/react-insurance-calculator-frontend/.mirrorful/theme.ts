
  export type Colors = keyof typeof Tokens.colors
  export type FontSize = keyof typeof Tokens.fontSizes
  export type Shadows = keyof typeof Tokens.boxShadows

  export type Token = Colors | FontSize | Shadows

  export const Tokens = {
  colors: {
    blue: {
      '50': '#dbc9ff',
      '100': '#c3a6ff',
      '200': '#ab82ff',
      '300': '#935eff',
      '400': '#8042ff',
      '500': '#681eff',
      '600': '#5200f9',
      '700': '#3d00b9',
      '800': '#310095',
      '900': '#250072',
      base: '#681eff',
    },
    'bright-green': {
      '50': '#d8ffc9',
      '100': '#beffa6',
      '200': '#a4ff82',
      '300': '#8aff5e',
      '400': '#75ff42',
      '500': '#5bff1e',
      '600': '#44f900',
      '700': '#32b900',
      '800': '#289500',
      '900': '#1f7200',
      base: '#5bff1e',
    },
    'bright-turquoise': {
      '50': '#c9fff0',
      '100': '#a6ffe5',
      '200': '#82ffdb',
      '300': '#5effd1',
      '400': '#42ffc8',
      '500': '#1effbe',
      '600': '#00f9b1',
      '700': '#00b984',
      '800': '#00956a',
      '900': '#007251',
      base: '#1effbe',
    },
    'torch-red': {
      '50': '#ffc9cf',
      '100': '#ffa6af',
      '200': '#ff828f',
      '300': '#ff5e6f',
      '400': '#ff4256',
      '500': '#ff1e36',
      '600': '#f9001b',
      '700': '#b90014',
      '800': '#950010',
      '900': '#72000c',
      base: '#ff1e36',
    },
  },
  fontSizes: {
    sm: '1rem',
    md: '1.2rem',
    lg: '1.4rem',
  },
  fontWeights: {
    light: '200',
    normal: '400',
    bold: '700',
  },
  lineHeights: {
    short: '1',
    normal: '1.5',
    tall: '2',
  },
  boxShadows: {
    sm: '1rem',
    md: '1.2rem',
    lg: '1.4rem',
  },
}
  