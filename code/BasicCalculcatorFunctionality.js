// premium => Prämie
// bonus / penalty level => Bonus / Malus Stufe

levelToPremiumPercentage = {"-3": 45,"0": 47.5,"8": 85, "9": 90, "10": 100, "11": 100, "12": 105, "13": 110, "14": 115, "15": 120, "16": 130 }

console.log("Level 10", levelToPremiumPercentage[10] + '%')
for (let n = 0; n < 10; n++){
    let premiumPercentage = levelToPremiumPercentage[10 + n] 
    premiumPercentage ??= levelToPremiumPercentage[Object.keys(levelToPremiumPercentage).sort((a, b) => parseFloat(a)-parseFloat(b)).pop()]
    console.log(`Level ${10 + n}`, premiumPercentage  + '%')
}