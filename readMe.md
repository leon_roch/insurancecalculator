# Insurance Calculator

Insurance Calculator is a web application that allows adults to decide whether a car damage should be reported to the car insurance company or not.

The premium of a car insurance varies depending on how many claims you report to the insurance company. Sometimes it is more worthwhile to pay for the damage yourself in order to have a lower premium. This is exactly what my calculator is for.

## Table of Contents

1. [Log](#log)
2. [Prerequisites](#prerequisites)
3. [Installing the Insurance Calculator](#installing-the-insurance-calculator)
4. [Using the Insurance Calculator](#using-the-insurance-calculator)
5. [Contact](#contact)
6. [License](#license)

## Log

[Planning Overview](./planning/Log.md)

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed node.js and npm.
- You have installed docker.

## Installing the Insurance Calculator

To install Insurance Calculator, follow these steps:

1. Download the [Dockerfile](./deployment/Dockerfile)
2. Fill in the environment variables in the environment file ([Template](./deployment/.env.example))
3. Build the docker image

```
docker build -t insurancecalculator:latest .
```

4. Run the docker image

```
docker run -d -p 80:80 --env-file <path/to/.env> insurancecalculator:latest
```

5. Open the application in your [browser](http://localhost)

## Using the Insurance Calculator

[Visit here](https://insurance-calculator-kbw.web.app/)

<!--
<usage_example>
Add run commands and examples you think users will find useful. Provide an options reference for bonus points!
-->

## Contact

If you want to contact me you can reach me at leonardroch44@gmail.com.

## License

This project uses the following license: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).
